package OSV::Client v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;

use Carp;
use HTTP::Tiny;
use JSON::PP qw(encode_json decode_json);
use Exporter qw(import);

our @EXPORT_OK = qw(query_commit query_version show_json);

my $url  = 'https://api.osv.dev/v1/query';

my $headers = {
    'Accept'       => 'application/json',
    'Content-Type' => 'application/json'
};


sub check_response {
    my $resp = shift;
    my $data;

    if ( $resp->{'success'} ) {
        $data = decode_json( $resp->{content} );
    } else {
        $data = undef;
        #carp 'Bad JSON response!';
    }

    return $data;
}


sub query_commit {
    my $hash = shift;
    my $data = { 'commit' => $hash };
    my $client = HTTP::Tiny->new;

    my $resp = $client->post( $url => {
        content => encode_json($data),
        headers => $headers
    });

    $data = check_response($resp);

    return $data;
}


sub query_version {
    my $version  = shift;
    my $pkg_name = shift;
    my $ecosys   = shift;

    my $data = {
        'version' => $version,
        'package' => {
            'name'      => $pkg_name,
            'ecosystem' => $ecosys
        }
    };

    my $client = HTTP::Tiny->new;

    my $resp = $client->post( $url => {
        content => encode_json($data),
        headers => $headers
    });

    $data = check_response($resp);

    return $data;
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Client.pm - OSV::Client module.

=head1 VERSION

This document describes OSV::Client version 0.7.

=head1 NOTE

Work in progress...

=head1 SYNOPSIS

use OSV::Client qw(query_commit show_json);

# using commit hash of Golang package 'nsqio' as an example

my $hash = 'd7acddb4babdf3329ad415cc02b605a239011b4b';
my $data = query_commit($hash);
show_json($data);
...

=head1 DESCRIPTION

The OSV::Client module acts as an OSV client to query OSV API via HTTP and
fetch package vulnerability data as JSON.

There are 2 different methods to query package data from OSV API;
the first method is via sending a query 'by commit hash',
and the second method is via sending a query by 'package version'.

Using the second method also requires 'package name and ecosystem'
fields to be specified before posting HTTP requests to OSV API.

If the response from the OSV API contains a malformed or empty JSON data, -
the current request is accepted as a failure.
Then the code throws an exception and exits.

The response is a JSON object - from which
package vulnerability information can be extracted.

=head1 INTERFACE

=over

=item

    * check_response()

      Definition: This is an helper function for internal usage.

    * query_commit()

      Definition: Sends a post request to OSV API and queries
                  the package vulnerability information by commit hash.

      Input:  Accepts $hash string
      Output: Returns JSON data/response object

    * query_version()

      Definition: Sends a post request to OSV API and queries
      the package vulnerability information by package version number.

      Input:  Accepts $version, $pkg_name, $ecosys parameters
      Output: Returns JSON data/response object

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
