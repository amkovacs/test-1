package Mod::Base v0.0.7;

# use at least Perl version 5.32
use v5.32;

# Uncomment next line to produce verbose warning diagnostics
#use diagnostics;

use strict;
use warnings;

use autodie ();
use feature ();
use utf8    ();
use mro     ();

use Cwd qw(abs_path);
use FindBin qw($Bin);


# All module globals are defined below

# shortcuts for command line arguments (i.e. @ARGV)
our $ARG  = '';
our $CONF = '';
our $MODE = '';

# default file path for AsciiDoc
our $GENDOC = "$Bin/../data/asciidocs/report.adoc";

# default config dir path
our $CONF_DIR = "$Bin/../data/configs";

# default log dir path
our $LOG_DIR = "$Bin/../data/logs";

# default dir path for cargo-deny logs
our $RSLOG_DIR = "$Bin/../data/logs/rust";

# default root dir path to be scanned
our $ROOT = abs_path( "$Bin/../../.." );

# default Rust dir path for cargo-deny check
our $RUST = abs_path( "$ROOT/../rust" );

# generated docs array
our @generated_docs;

# Maps build identifier from build definition to a human-readable build name
# (which is later included into the documentation)

our %BUILD_NAMES = (
    'ALL'             => 'All',
    'aix'             => 'AIX',
    'aix6'            => 'AIX 6',
    'fbsd11-amd64'    => 'FreeBSD 11 x64',
    'generic'         => 'Generic RPM and DEB',
    'generic-glibc25' => 'Generic GLIBC 2.5',
    'macos'           => 'MacOS',
    'macos-arm64'     => 'MacOS x64',
    'solaris10-sparc' => 'Solaris 10 SPARC',
    'solaris10-i386'  => 'Solaris 10 i386',
    'win64'           => 'Windows x64',
);

# %libinfo data provides backwards compatibility info
our %libinfo;

# Hash linking source maps to an array of builds it is included in
# e.g. oldssl => [ aix, aix6 ]
our %source_map;


# Secures %ENV global hash at compile time.
BEGIN {
    $ENV{PATH} = '/bin:/usr/bin';
    $ENV{SHELL} = '/bin/bash' // '/bin/sh' if exists $ENV{SHELL};
    # sanitize environment variables that cause weird bugs
    delete @ENV{ qw( IFS CDPATH ENV BASH_ENV LD_LIBRARY_PATH ) };
}


# Loads required pragmas into current name space and
# auto-imports common variables.
sub import {
    strict->import();
    warnings->import();
    autodie->import();
    feature->import(':5.32');
    utf8->import();
    mro::set_mro( scalar caller(), 'c3' );

    no strict 'refs';
    my $mod = caller;

    *{$mod . "::ARG"}       = \$ARG;
    *{$mod . "::CONF"}      = \$CONF;
    *{$mod . "::MODE"}      = \$MODE;
    *{$mod . "::GENDOC"}    = \$GENDOC;
    *{$mod . "::CONF_DIR"}  = \$CONF_DIR;
    *{$mod . "::LOG_DIR"}   = \$LOG_DIR;
    *{$mod . "::RSLOG_DIR"} = \$RSLOG_DIR;
    *{$mod . "::ROOT"}      = \$ROOT;
    *{$mod . "::RUST"}      = \$RUST;
    *{$mod . "::generated_docs"} = \@generated_docs;
    *{$mod . "::BUILD_NAMES"}    = \%BUILD_NAMES;
    *{$mod . "::libinfo"}        = \%libinfo;
    *{$mod . "::source_map"}     = \%source_map;
}


# Cleans loaded pragmas from current name space.
sub unimport {
    strict->unimport;
    warnings->unimport;
    autodie->unimport;
    feature->unimport;
    utf8->unimport;
}

1;
__END__

=pod

=encoding utf8

=head1 NAME

Base.pm - Mod::Base module.

=head1 VERSION

This document describes Mod::Base version 0.7.

=head1 SYNOPSIS

use Mod::Base;

=head1 DESCRIPTION

Mod::Base module implements core functionality and
package/module globals for all sec_check modules/tests/scripts.

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
