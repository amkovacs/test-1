package Manifest::Parser v0.0.7;

use Carp;
use List::Util qw(any);
use POSIX qw(strftime mktime);

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Util::Log;
use Manifest::File;

# import get_cves from NVD::Client
use NVD::Client qw(get_cves);

# import query_commit function from OSV::Client module
#use OSV::Client qw(query_commit);


# Manifest validation schema is pretty self-explaining.
# Types supported are: string, array, hashmap.
# 'match' is a regex. Use qr/./ as 'non-empty' condition.
# For 'array' type with 'match' directive, every element of array is checked against 'match' expression.
# 'schema' defines sub-schema for 'hashmap' and 'array' types.

my $MANIFEST_SCHEMA = {
    name        => { required => 1,                                   type => 'string',              match => qr/./                             },
    cpe23       => {                                                  type => 'string',              match => qr/^cpe:2\.3:[aoh]:[\w-]*:[\w-]+/ },
    type        => { required => 1,                                   type => 'string',              match => qr/^(?:external|embedded)$/       },
    buildSource => { required => sub { $_[0]->{type} eq 'external' }, type => [ 'string', 'array' ], match => qr/^\w+$/                         },
    version     => { required => sub { $_[0]->{type} eq 'embedded' }, type => [ 'string', 'array' ], match => qr/./                             },
    versionMap  => {                                                  type => [ 'string', 'array' ], match => qr/^[\w\.-]+\s*->\s*[\w\.-]+$/    },
    reviewer    => { required => sub { !$_[0]->{cpe23} },             type => 'string',              match => qr/@/                             },
    reviewDate  => { required => sub { !$_[0]->{cpe23} },             type => 'string',              match => qr/^\d{4}-\d\d-\d\d$/             },
    comment     => {                                                  type => 'string'                                                          },
    CVEs        => { type => 'hashmap', keymatch => qr/^CVE-\d{4}-\d{4,}$/, schema => {
    status      => { required => 1, type => 'string', match => qr/^(?:Not\sapplicable|Observed|Active)$/i },
    description => { required => 1, type => 'string', match => qr/./                                      },
    reviewer    => { required => 1, type => 'string', match => qr/@/                                      },
    reviewDate  => { required => 1, type => 'string', match => qr/^\d{4}-\d\d-\d\d$/                      },
    comment     => {                type => 'string'                                                      },

    } }
};


# Maps type from validation schema to Perl reference type
my %TYPEMAP = (
    'string'  => '',
    'array'   => 'ARRAY',
    'hashmap' => 'HASH',
);

my $NOW = strftime('%Y-%m-%d', localtime);


sub validate_format {
    my ( $manifest_clause, $manifest, $schema) = @_;
    $schema = $MANIFEST_SCHEMA unless defined $schema;

    for my $k ( keys %$schema ) {
        my $sch = $schema->{$k};

        carp "Required key '$k' is absent in $manifest_clause\n"
            if !ref $sch->{required} && $sch->{required} && !exists $manifest->{$k} ||
               ref $sch->{required} eq 'CODE' && $sch->{required}->($manifest) && !exists $manifest->{$k};
    }

    for my $k ( keys %$manifest ) {
        my $sch = $schema->{$k};
        carp "Key '$k' is not known in $manifest_clause\n" unless $sch;

        my %types;

        if ( !ref $sch->{type} ) {
            $types{ $TYPEMAP{ $sch->{type} } } = 1;
        } elsif ( ref $sch->{type} eq 'ARRAY' ) {
            $types{ $TYPEMAP{$_} } = 1 for $sch->{type}->@*;
        }

        carp "Value type for key '$k' is not valid in $manifest_clause\n"
            unless $types { ref $manifest->{$k} };

        my $validate_scalar = sub {
            my ( $sch, $val ) = @_;
            carp "Value '$val' is not valid for key '$k' in $manifest_clause\n"
                if $sch->{match} && $val !~ $sch->{match};
        };

        if ( !ref $manifest->{$k} ) {
            $validate_scalar->( $sch, $manifest->{$k} );
        } elsif ( ref $manifest->{$k} eq 'ARRAY' ) {
            for ( $manifest->{$k}->@* ) {
                if ( $sch->{schema} ) {
                    validate_format($manifest_clause, $_, $sch->{schema} );
                } else {
                    $validate_scalar->($sch, $_);
                }
            }
        } elsif ( ref $manifest->{$k} eq 'HASH' ) {
            while ( my ( $hk, $hv ) = each $manifest->{$k}->%* ) {
                carp "Key '$hk' under '$k' is not valid in $manifest_clause\n"
                if $sch->{keymatch} && $hk !~ $sch->{keymatch};

                validate_format($manifest_clause, $manifest->{$k}{$hk}, $sch->{schema}) if $sch->{schema};
            }
        }
    }
}


sub format_description {
    my $desc = shift;
    my $res;

    for (@$desc) {
        $res = $_->{value} and last if $_->{lang} eq 'en';
    }

    unless ($res) {
        for (@$desc) {
            $res = $_->{value} and last if !$_->{lang};
        }

        $res = $desc->[0]{value} unless $res; # Better than nothing
    }

    $res =~ s/~/\\~/g;
    return $res;
}


# Given a manifest, returns a reference to a hash mapping library versions to
# an array of build identifiers which are using that version
sub versions_builds {
    my $manifest = shift;
    my ( %versions, @versions, %version_to_sourcesets, %version_map );

    if ( $manifest->{type} eq 'external' ) {
        for my $libid ( Manifest::File::getmark_libs($manifest) ) {
            for my $ver ( keys $libinfo{$libid}->%* ) {
                $ver =~ s/^=//;
                push @versions, $ver;
                $version_to_sourcesets{$ver} = $libinfo{$libid}{$ver};
            }
        }
    } elsif ( $manifest->{type} eq 'embedded' ) {
        @versions = ref $manifest->{version} eq 'ARRAY'
        ? $manifest->{version}->@*
        : ( $manifest->{version} );
    }

    if ( $manifest->{versionMap} ) {
        my @version_map = ref $manifest->{versionMap} eq 'ARRAY'
            ? $manifest->{versionMap}->@*
            : ( $manifest->{versionMap} );

        for (@version_map) {
            my ( $from, $to ) = /^([\w\.-]+)\s*->\s*([\w\.-]+)$/;
            $version_map{$from} = $to;
        }
    }

    for my $ver (@versions) {
        my @builds_affected = ('ALL');

        if ( $manifest->{type} eq 'external' && !$version_to_sourcesets{$ver}{default} ) {
            my %builds;

            for my $sourceset ( keys $version_to_sourcesets{$ver}->%* ) {
                $builds{$_} = 1 for $source_map{$sourceset}->@*;
            }

            @builds_affected = sort keys %builds;
        }

        $versions{ $version_map{$ver} // $ver } = \@builds_affected;
    }

    return \%versions;
}


sub check_data {
    my ( $manifest, $manifest_relpath ) = @_;

    unless ( $manifest->{cpe23} ) {
        Util::Log::write_debug("Found managed dependency '$manifest->{name}' without a CPE");

        # Just mark libs as met
        Manifest::File::getmark_libs($manifest);

        Util::Log::write_line( "Manifest for '$manifest->{name}' in $manifest_relpath has a review date in the future")
            if $manifest->{reviewDate} gt $NOW;

        my $next_review_date = Manifest::File::next_review_date($manifest->{reviewDate});

        Util::Log::write_debug( "  Next review date: $next_review_date" );

        Util::Log::write_line( "Manifest for '$manifest->{name}' in $manifest_relpath should be reviewed as of $next_review_date" )
            if $NOW ge $next_review_date;

        return;
    }

    my @cpe_query = split /:/, $manifest->{cpe23};
    my $versions  = versions_builds($manifest);

    for my $ver ( sort keys %$versions ) {
        my $cpe = join ':', @cpe_query[ 0 .. 4 ], $ver;
        Util::Log::write_debug "  Requesting CVEs with CPE $cpe for '$manifest->{name}' version $ver";
        my $cves = NVD::Client::get_cves($cpe);

        if ( $cves->{totalResults} ) {
            for my $cve ( $cves->{result}{CVE_Items}->@* ) {
                my $cveid = $cve->{cve}{CVE_data_meta}{ID};

                if ( $manifest->{CVEs} && $manifest->{CVEs}{$cveid} ) {
                    Util::Log::write_debug( "    Found managed $cveid" );

                    if ( any { $manifest->{CVEs}{$cveid}{status} eq $_ } qw(Active Observed) ) {
                        Util::Log::write_line( "$cveid for '$manifest->{name}' in $manifest_relpath has a review date in the future" )
                            if $manifest->{CVEs}{$cveid}{reviewDate} gt $NOW;

                        my $next_review_date = Manifest::File::next_review_date( $manifest->{CVEs}{$cveid}{reviewDate} );

                        Util::Log::write_debug( "    Next review date: $next_review_date" );

                        Util::Log::write_line( "$cveid for '$manifest->{name}' in $manifest_relpath should be reviewed as of $next_review_date" )
                            if $NOW ge $next_review_date;
                    }

                    if ( $manifest->{CVEs}{$cveid}{status} eq 'Active' && $GENDOC ) {
                        my $descr = format_description( $cve->{cve}{description}{description_data} );
                        push @generated_docs, Manifest::File::gendoc( $cveid, $manifest, $versions->{$ver}, $descr );
                    }
                } else {
                    Util::Log::write_line( "Unmanaged $cveid found for '$manifest->{name}', version $ver, in $manifest_relpath, " .
                              "builds affected: " . join( ', ', $versions->{$ver}->@* ) .
                              " (see https://nvd.nist.gov/vuln/detail/$cveid)" );
                }
            }
        }
    }
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Parser.pm - Manifest::Parser module.

=head1 VERSION

This document describes Manifest::Parser version 0.7.

=head1 NOTE

This is an internal submodule used by System::Dirs.

=head1 SYNOPSIS

use Manifest::Parser qw(format_description validate_format);
...

=head1 DESCRIPTION

Manifest::Parser module parses/validates format descriptions of manifest files.

=head1 INTERFACE

=over

=item Manifest::Parser

    * check_data()

      Definition: Checks all data in a single manifest file. Parameters are manifest itself and
                  manifest file path (for logging). Dies on failure. Return value is undefined.

      Input:  Receives manifest file and manifest filepath strings
      Output: ...

    * format_description()

      Definition: Formats a human-readable string describing a vulnerability,
                  by the way hacking CVE markdown to present a valid AsciiDoc.


      Input:  Receives CVE {description}{description_data} hash data
      Output: Returns a human-readable string describing a vulnerability

    * validate_format()

      Definition: Validates a single manifest. Dies on failure.

      Input:  Receives $manifest_clause, $manifest, $schema strings
      Output: ...

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
