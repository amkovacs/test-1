package Manifest::File v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Util::Log;
use Build::Definition;

use Carp;
use POSIX qw(strftime mktime);

# set timing of periodic CVE reviews
my $REVIEW_EVERY_MONTHS = 2;

my %libids_met;


sub check_dep {
    for (keys %libinfo) {
        Util::Log::write_line( "Build dependency '$_' is mentioned in the build definition but is not managed by any security manifest" )
            unless $libids_met{$_};
    }

    if (defined $GENDOC) {
        open my $file, '>', $GENDOC or carp "Cannot open $GENDOC for writing: $!\n";
        print $file $_ for @generated_docs;
        close $file;
        $file = undef;
    }
}


sub gendoc {
    my ( $cveid, $manifest, $builds, $descr ) = @_;

    my $res = "=== https://nvd.nist.gov/vuln/detail/${cveid}[$cveid]\n\n";

    $res .= "Component affected:: $manifest->{name}\n\n";
    $res .= "Builds affected:: " . join(', ', map { $BUILD_NAMES{$_} // $_ } @$builds) . "\n\n";
    $res .= "$descr\n\n";
    $res .= "'''\n\n";

    return $res;
}


sub getmark_libs {
    my $manifest = shift;

    if ( $manifest->{buildSource} ) {
        my @libids
            = ref $manifest->{buildSource} eq 'ARRAY'
            ? $manifest->{buildSource}->@*
            : ( $manifest->{buildSource} );

        $libids_met{$_} = 1 for @libids;

        return @libids;
    }

    ();
}


sub next_review_date {
    my @review_date = split /-/, shift;

    strftime( '%Y-%m-%d', localtime(
        mktime(
            0, 0, 0, $review_date[2],
            $review_date[1] - 1 + $REVIEW_EVERY_MONTHS,
            $review_date[0] - 1900
        )
    ));
}

1;
__END__

=pod

=encoding utf8

=head1 NAME

Check.pm - Manifest::File module.

=head1 VERSION

This document describes Manifest::File version 0.7.

=head1 NOTE

...

=head1 SYNOPSIS

use Manifest::File qw(check_dep);
...

=head1 DESCRIPTION

The Manifest::File module manages manifest files metadata.

It checks if a manifest file exists for the relevant library/package.
If it can't find one, then it fetches an updated version of the manifest file
from NVD and OSV APIs.

=head1 INTERFACE

=over

=item Manifest::File

    * check_dep()

      Definition: Check if there's a dependency in build definition
                  which is not present in any security manifest.

      Input:  ...
      Output: ...

    * getmark_libs()

      Definition: Given a manifest, extracts all the library identifiers
                  (buildSource-s) from it, marks them met and returns them as a list

      Input:  Receives $manifest path string
      Output: Returns @libids array

    * next_review_date()

      Definition: Given a review date (YYYY-MM-DD string) calculates the next
                  review date, adding $REVIEW_EVERY_MONTHS months

      Input:  Receives $manifest->{reviewDate} string
      Output: Returns @review_date array

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
