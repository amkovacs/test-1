package System::Cmd v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Util::Log;
use Util::Shell qw(run);
use Pod::Usage qw(pod2usage);


sub parse {
    $MODE = $ARGV[0] // '--build';
    $ARG  = $ARGV[1];

    if ( $MODE =~ /--shell/ ) {

        $MODE = 'shell';
        Util::Shell::run();

    } elsif ( $MODE =~ /--build|--gendoc|--log/ ) {

        $MODE = 'build';
        $CONF = 'dep';

    } elsif ( $MODE =~ /--cargo|--rust/ ) {

        $MODE = 'cargo';
        $ARG  = 'offline';

    } elsif ( $MODE =~ /--help|help|--usage|usage/ ) {

        $MODE = 'help';
        pod2usage();

    } else {

        $MODE = 'help';
        pod2usage( -verbose => 2 );

    }

    unless ( $MODE =~ /help|shell/ ) {

        Util::Log::open_log();
        Util::Log::open_debug();

    }

}


sub sec_exit {
    Util::Log::close_all();
    my $pcnt = Util::Log::counter();

    # print a warning and exit program securely
    # if any issues are found after the recursive directory scan

    if ( defined $pcnt ) {
        say "[info] Security issues or vulnerabilities found!";
        say "[info] Please check the log files in '$Bin/data/logs/' directory for more information.";
    } else {
        say "[info] No security issues or vulnerabilities found!";
        say "[info] Scan completed with success!";
    }

    exit 0;
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Cmd.pm - System::Cmd module.

=head1 VERSION

This document describes System::Cmd version 0.7.

=head1 SYNOPSIS

use System::Cmd;
...

=head1 DESCRIPTION

System::Cmd module parses command line parameters and
displays command line usage/documentation.

=head1 INTERFACE

=over

=item System::Cmd

    * parse()

      Definition: Parses command line parameters and displays command usage.

      Input:  None
      Output: Prints command line usage docs

    * sec_exit()

      Definition: Closes open filehandles etc. and exits the program in a secure manner.

      Input:  None
      Output: Prints info messages about the scan results

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
