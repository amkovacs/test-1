package System::Dirs v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Cargo::Deny;
use Cargo::Config;
use Util::Git;
use Util::IO;
use Util::Log;
use Manifest::Parser;
use OSV::Client;

use Carp;
use Cwd qw(abs_path);


sub scan_src {
    my $dir = shift;

    my $manifest_file = "./SecurityManifest.yml";

    if (defined $dir) {
        $manifest_file = "$dir/SecurityManifest.yml";
    }

    unless (-f $manifest_file) {
        Util::Log::write_debug("[info] No manifest file found");
        Util::Log::write_debug("[info] Checking if this is a git repo...");

        my $hash = Util::Git::parse_commit($dir);
        #my $data = OSV::Client::query_commit($hash) if defined $hash;
        #my $filepath = Util::IO::save_json($data, $hash) if defined $data;

        #Util::Log::write_debug("  OSV data saved to $filepath") if defined $filepath;
        return;
    }

    my $manifest_relpath = Util::IO::relpath($manifest_file);
    my @manifests = Util::IO::load_yaml($manifest_file) or carp "Cannot read $manifest_relpath\n";
    carp "Empty security manifest $manifest_relpath\n" unless @manifests;

    for my $manifest (@manifests) {
        next unless %$manifest; # Allow totally empty manifests, as a separator

        my $manifest_clause = (
            $manifest->{name} && !ref $manifest->{name}
            ? "manifest '$manifest->{name}'"
            : 'unnamed manifest'
        ) . " in $manifest_relpath";

        Manifest::Parser::validate_format($manifest_clause, $manifest);
        Manifest::Parser::check_data($manifest, $manifest_relpath);
    }
}


sub walk_src {
    my $dir = shift;

    if (defined $dir) {
        Util::Log::write_debug("Entering $dir");
        scan_src($dir);
    } else {
        return;
    }

    opendir(my $dh, $dir) or carp "Cannot open $dir: $!\n";

    while ( readdir $dh ) {
        next if /^\./;
        my $entry = "$dir/$_";
        walk_src($entry) if -d $entry;
    }

    closedir $dh;
}


sub scan_rust {
    my $dir = shift;

    Util::Log::write_debug("[info] Checking if this is a Rust lib source directory...");

    if ( Cargo::Config::valid_lib( $dir ) ) {
        chdir $dir;

        if ( $ARG eq 'offline' ) {
            Cargo::Deny::check_offline($dir);
        } else {
            Cargo::Deny::check_online($dir);
        }
    }
}


sub walk_rust {
    my $dir = abs_path(shift);

    if (defined $dir) {
        Util::Log::write_debug("[info] Entering $dir");
        scan_rust($dir);
    } else {
        return;
    }

    opendir(my $dh, $dir) or carp "Cannot open $dir: $!\n";

    while ( readdir $dh ) {
        # skip walking through the following directory patterns
        next if /^\.|^ci|^data|^doc|^media|^out|^target|^test|^them/;
        my $entry = "$dir/$_";
        walk_rust($entry) if -d $entry;
    }

    closedir $dh;
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Dirs.pm - System::Dirs module.

=head1 VERSION

This document describes System::Dirs version 0.7.

=head1 NOTE

While scanning the directories for manifest files,
the hidden directories (starting with '.') are ignored.

=head1 SYNOPSIS

use System::Dirs;
...

=head1 DESCRIPTION

System::Dirs module implements functions for walking directories and
scanning their contents for manifest files in a recursive way.

System::Dirs module makes use of the Manifest::* submodules' functions
to scan, manage files.

It checks if a manifest file exists for the relevant source library/package.

=head1 INTERFACE

=over

=item System::Dirs

    * scan_src()

      Definition: Recursively scans a single working directory.
                  Silently returns if no manifest file found.
                  Dies if manifest file is empty, unreadable or is not a valid YAML,
                  or if it fails to validate the manifest.
                  Found problems are logged to DEBUG log.

      Input:  Receieves $dir string representing the current directory path
      Output: Returns nothing

    * walk_src()

      Definition: Recursively walks current directory tree one by one.
                  Calls scan() function for each directory.

      Input:  Receieves $dir string representing the directory path
      Output: Returns nothing

    * scan_rust()

      Definition: Recursively scans a signle Rust directory.
                  Runs cargo-deny checks for each Rust lib source directory.
                  Found problems are logged to 'data/logs/rust/*' for each library.

      Input:  Receieves $dir string representing the directory path
      Output: Returns nothing

    * walk_rust()

      Definition: Recursively walks Rust directory tree one by one.
                  Calls scan_rust() function for each directory.

      Input:  Receieves $dir string representing the directory path
      Output: Returns nothing

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
