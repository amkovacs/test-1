package System::OS v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Perl::OSType ':all';


sub detect_distro {
    my $os = os_type();
    my $distro = '';
    my $cmd = can_run('lsb_release');

    if ( $os eq 'Unix' and defined $cmd ) {
        $cmd = 'lsb_release --id';
        my $release = run($cmd);

        $release =~ /(\S+)\s+$/;
        $distro = $1;
    } elsif ( $os eq 'Windows' ) {
        $distro = 'MSYS2';
    }

    return $distro;
}


sub can_run {
    my $cmd = shift;

    my $bin = "which $cmd";
    my $out = run($bin);

    if ( defined ($out) ) {
        return 1;
    }

    return 0;
}


sub run {
    my $cmd = shift;
    my $out = '';

    $cmd = "$cmd 2>&1";

    if ( is_os_type( 'Unix', 'linux' ) ) {

        my $bash = "bash -l -c";
        $out = qx( $bash "$cmd" );

    } elsif ( is_os_type( 'Unix', 'freebsd' ) ) {

        $out = qx( $cmd );

    }

    return $out;
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

OS.pm - System::OS module.

=head1 VERSION

This document describes System::OS version 0.7.

=head1 SYNOPSIS

use System::OS;

my $cmd = "ls -l";
my $out = System::OS::run($cmd);

print $out;

=head1 DESCRIPTION

System::OS module implements advanced shell system command execution and
OS/distro detection functionality.

=head1 INTERFACE

=over

=item System::OS

    * detect()

      Definition: Detects installed OS type and distro.

      Input:  Not required
      Output: Returns $distro string

    * can_run()

      Definition: Checks if the given command is available on the system.

      Input:  Receives $dms string
      Output: Returns true or false

    * run()

      Definition: Runs the given shell command using the OS/distro specific shell interpreter.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
