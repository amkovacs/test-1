package Cargo::Config v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Util::IO qw(copy_file);
use Cwd qw(getcwd);


sub init {
    my $dir = shift;
    chdir $dir;

    unless ( -f "deny.toml" ) {
        my $config = "$CONF_DIR/deny.toml";
        copy_file($config, $dir);
    }
}


sub valid_lib {
    my $dir = shift // getcwd();

    if ( -f "$dir/Cargo.lock" or -f "$dir/Cargo.toml" ) {
        return 1;
    }

    return 0;
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Dir.pm - Cargo::Config module.

=head1 VERSION

This document describes Cargo::Config version 0.7.

=head1 SYNOPSIS

use Cargo::Config;
...

=head1 DESCRIPTION

Cargo::Config implements configs for Rust
library source directories.

=head1 INTERFACE

=over

=item Cargo::Config

      * init()

      Definition: Initializes cargo_deny configs for Rust lib directories.

      Input:  Not required
      Output: None

    * valid_lib()

      Definition: Detects if current directory includes a Rust crate/lib.

      Input:  Not required
      Output: Returns true or false.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
