package Cargo::Deny v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use System::OS;
use Util::IO qw(dirname);
use Util::Log;


sub init {
    chdir $RUST;
    my $cmd = 'cargo deny init';
    System::OS::run($cmd);
}


sub genlog {
    my $path = shift;
    my $cmd  = shift;

    chdir $path;
    my $lib = dirname($path);
    my $ext = '.log';

    my $log = $RSLOG_DIR . '/' . $lib . $ext;
    my $out = System::OS::run($cmd);
    Util::Log::save($log, $out);
}


sub check_online {
    my $dir = shift;
    my $cmd = "cargo deny check advisories";

    genlog($dir, $cmd);
}


sub check_offline {
    my $dir = shift;
    my $cmd = 'cargo deny --offline check advisories';

    genlog($dir, $cmd);
}


sub check_bans {
    my $dir = shift;
    my $cmd = 'cargo deny check bans';

    genlog($dir, $cmd);
}


sub check_licenses {
    my $dir = shift;
    my $cmd = 'cargo deny check licenses';

    genlog($dir, $cmd);
}


sub check_sources {
    my $dir = shift;
    my $cmd = 'cargo deny check sources';

    genlog($dir, $cmd);
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Deny.pm - Cargo::Deny module.

=head1 VERSION

This document describes Cargo::Deny version 0.7.

=head1 NOTE

Please see Rust-sec developers' security advisories network for more info:

https://rustsec.org

=head1 SYNOPSIS

use Cargo::Deny;
...

=head1 ABSTRACT

...

=head1 DESCRIPTION

Cargo::Deny module implements functions for querying/checking the Rust developers'
advisories database to detect issues for local Rust crates/libs
with the help of "cargo deny" command.

=head1 INTERFACE

=over

=item Cargo::Deny

    * check_advisory()

      Definition: Detects issues for Rust crates/libs by looking in an advisory database.

      Input:  Not required
      Output: Returns $info string.

    * check_licenses()

      Definition:  Verifies that every crate you use has license terms you find acceptable.

      Input:  Not required
      Output: Returns $info string.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
