package Util::IO v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;

use Cwd qw(abs_path);
use Data::Dumper;

use File::Basename qw(basename);
use File::Copy qw(copy);
use File::Spec::Functions qw(abs2rel);
use YAML qw(LoadFile);

use Exporter qw(import);

# the following functions and variables are exported only by request.
our @EXPORT_OK = qw(copy_file dirname load_yaml relpath read_file show_data save_json);


sub dirname {
    my $path = shift;
    my $dir = basename($path);

    return $dir;
}


sub relpath {
    my $dir = shift;

    return abs2rel(abs_path($dir), $ROOT);
}


sub copy_file {
    my $src = shift;
    my $dst = shift;

    copy($src, $dst);
}


sub load_yaml {
    my $filename = shift;
    my $yaml = LoadFile($filename);

    return $yaml;
}


sub read_file {
    my $filepath = shift;
    open(my $file, "<", $filepath);

    my $data = '';

    while ( my $line = readline $file ) {
        $data .= $line;
    }

    close $file;
    $file = undef;

    return $data;
}


sub save_json {
    my $data = shift;
    my $hash = shift;
    my $filepath = "$Bin/data/manifests/$hash.json";

    open(my $file, ">", $filepath);
    say $file Dumper $data;

    close $file;
    $file = undef;

    return $filepath;
}


sub show_data {
    my $data = shift;
    my $tag = shift;

    $Data::Dumper::Terse = 1;
    say $tag . " DATA:\n" . Dumper $data;
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

IO.pm - Util::IO module.

=head1 VERSION

This document describes Util::IO version 0.7.

=head1 NOTE

Work in progress...

=head1 SYNOPSIS

use Util::IO qw(load_yaml read_file);
...

=head1 DESCRIPTION

Util::IO module implements filesystem I/O functions for reading/loading/saving
files/dirs/paths on the filesystem.

=head1 INTERFACE

=over

=item Util::IO

    * dirname()

      Definition: Extracts base directory name from given path.

      Input:  Receives $path string
      Output: Returns $dir string

    * copy_file()

      Definition: Copies given $src file to $dst path.

      Input:  Receives $src filename and $dst dir name
      Output: Returns nothing

    * read_file()

      Definition: Reads given $filepath and returns file content.

      Input:  Receives $filepath string
      Output: Returns $data string

    * load_yaml()

      Definition: Loads YAML file

      Input:  Accepts $filename string
      Output: Returns YAML data

    * relpath()

      Definition: Converts absolute path to real path

      Input:  Accepts path string
      Output: Returns path string

    * show_data()

      Definition: Shows a map of nested-data such as JSON, HASH ref etc. in
                  a pretty-printed format.

      Input:  Accepts raw JSON $data
      Output: Returns formatted JSON data

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
