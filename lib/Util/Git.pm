package Util::Git v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Util::IO qw(read_file);

use Exporter qw(import);

our @EXPORT_OK = qw(is_git parse_commit);


sub is_git {
    my $dir = shift;

    if (-d "$dir/.git" and -f "$dir/.git/refs/heads/master") {
        return 1;
    } else {
        return 0;
    }
}


sub parse_commit {
    my $dir = shift;
    chdir $dir;

    if (is_git($dir)) {
        my $hash = read_file("$dir/.git/refs/heads/master");
        chomp $hash;
        return $hash;
    } else {
        return undef;
    }
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Git.pm - Util::Git module.

=head1 VERSION

This document describes Util::Git version 0.7.

=head1 NOTE

Work in progress...

=head1 SYNOPSIS

use Util::Git qw(parse_commit);
...

=head1 DESCRIPTION

The Util::Git module implements utility functions for extracting
and querying info from a local Git repository on the system.

=head1 INTERFACE

=over

=item Util::Git

    * is_git()

      Definition: Checks if current directory is a local Git repo.

      Input:  Receives $dir string
      Output: Returns true or false

    * parse_commit()

      Definition: Parses latest commit hash from a local Git repo.

      Input:  Receives $dir string
      Output: Returns git commit hash string

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
