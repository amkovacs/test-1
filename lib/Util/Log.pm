package Util::Log v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use POSIX qw(strftime);


# log file handles
my $DEBUG;
my $LOG;


sub counter {
    state $num = 0;
    return ++$num;
}


sub open_log {
    my $filepath = shift // "$LOG_DIR/report.log";
    open($LOG, ">>", $filepath);

    my $date_time = strftime "[%Y.%m.%0e-%H:%M:%S]", localtime;
    my $msg = "\n[INFO] Started security check on " . $date_time . "\n";
    say $LOG $msg;
}


sub open_debug {
    my $filepath = shift // "$LOG_DIR/debug.log";
    open($DEBUG, ">>", $filepath);

    my $date_time = strftime "[%Y.%m.%0e-%H:%M:%S]", localtime;
    my $line = "\n[INFO] Started debugging on " . $date_time . "\n";
    write_debug($line);
}


sub write_debug {
    my $line = shift;

    unless (defined $DEBUG) {
        open_debug();
    }

    say $DEBUG $line;
}


sub write_line {
    my $line = shift;

    if (defined $LOG) {
        my $num = counter();
        say $LOG "PROBLEM #" . ($num) . ": $line";
    }
}


sub save {
    my $filename = shift;
    my $data = shift;

    open(my $log, ">", $filename);
    say $log $data;

    close $log;
    $log = undef;
}


sub close_all {
    close $DEBUG if defined $DEBUG;
    close $LOG if defined $LOG;

    $DEBUG = undef;
    $LOG   = undef;
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Log.pm - Util::Log module.

=head1 VERSION

This document describes Util::Log version 0.7.

=head1 NOTE

Do not modify this module to change log messages.
Instead use a $msg string argument within the function caller code.

=head1 SYNOPSIS

use Util::Log;

Util::Log::open_log("data/logs/report.log");

my $msg = 'Put your log message here...';
Util::Log::write_line($msg);

Util::Log::close_all();

=head1 DESCRIPTION

The Util::Log module implements utility functions for logging.

=head1 INTERFACE

=over

=item Util::Log

    * save()

      Definition: Saves provided data to a separate log file.

      Input:  Receives $filename and $data strings
      Output: None

    * write_debug()

      Definition: Writes the message to debug log file.

      Input:  Receives $line string
      Output: None

    * write_line()

      Definition: Writes the message to report log file.

      Input:  Receives $line string
      Output: None

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
