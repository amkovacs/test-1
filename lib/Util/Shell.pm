package Util::Shell v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Cargo::Deny;
use Util::Git qw(parse_commit);
use Util::IO qw(read_file show_data);
use OSV::Client qw(query_commit);

use Cwd qw(abs_path getcwd);
use Term::ANSIColor;
use Exporter qw(import);

our @EXPORT_OK = qw(run);


sub banner {
    my $banner = qq(
_____________________
  _____ _     _      |
 |  ___(_)___| |__   |
 | |_  | / __| '_ \\  |
 |  _| | \\__ \\ | | | \|
 |_|   |_|___/_| |_| |
_____________________|
);
    return $banner;
}


sub print_color {
    my $text = shift;
    print color('green on_black');
    print $text;
    print color('reset');
}


sub say_color {
    my $text = shift;
    print color('green on_black');
    say $text;
    print color('reset');
}


sub fallback {
    my $cmd = shift // ' ';
    say_color("[info] Unknown command: $cmd");
}


sub cd {
    my $dir = shift // $Bin;
    chdir $dir;

    my $cwd = getcwd();
    my $msg = "\n[info] In $cwd\n";

    say_color($msg);
}


sub ls {
    my $dir = shift // ".";
    my @files = glob("$dir/*");

    say_color('');

    foreach my $file (@files) {
        say_color($file);
    }

    say_color('');
}


sub pwd {
    my $dir = getcwd();
    say_color("\n" . $dir . "\n");
}


sub clear {
    # clear the screen
    print "\033[2J";
    # jump to 0, 0
    print "\033[0;0H";
}


sub help {
    my $msg = "\nThe list of builtin commands and their parameters:\n\n";
    $msg   .= "  cd  [dir] -- Changes directory\n";
    $msg   .= "  ls  [dir] -- Lists directory contents\n";
    $msg   .= "  hash.get [dir] -- Shows commit hash inside a .git directory\n";
    $msg   .= "  nvd.get  [commit_hash] -- Gets CVE data from NVD API\n";
    $msg   .= "  osv.get  [commit_hash] -- Gets vulnerability data from OSV API\n";
    $msg   .= "  pwd   -- Prints working/current directory\n";
    $msg   .= "  clear -- Clears the screen\n";
    $msg   .= "  exit  -- Exits interactive shell\n";

    say_color($msg);
}


sub cargo_deny {
    my $info = Cargo::Deny::check_advisory();
    say_color("\n" . $info . "\n");
}


sub hash_get {
    my $dir = shift // "./";
    my $hash = parse_commit($dir);
    say_color("\n" . $hash . "\n") if defined $hash;
}


sub nvd_get {
    my $cpe = shift;
    my $data = get_cves($cpe);
    show_json($data);
}


sub osv_get {
    my $hash = shift;
    my $data = query_commit($hash);
    show_json($data);
}


sub parse {
    my $input = shift;
    my ($cmd, $arg) = split(" ", $input);
    $cmd = '' unless defined $cmd;

    my $table = {
        'help'       => \&help,
        'clear'      => \&clear,
        'cd'         => \&cd,
        'ls'         => \&ls,
        'pwd'        => \&pwd,
        'cargo.deny' => \&cargo_deny,
        'hash.get'   => \&hash_get,
        'nvd.get'    => \&nvd_get,
        'osv.get'    => \&osv_get,
    };

    if ( exists $table->{$cmd} ) {
        return $table->{ $cmd }->($arg);
    } else {
        fallback($cmd);
    }
}


sub run {
    my $mode = 'shell';
    my $input;
    my $prompt = '>>> ';

    my $intro = "Welcome to 'Fish' interactive shell.\n";
    $intro   .= "Type \"help\" to see the list of builtin commands.\n";

    say_color(banner());

    say_color($intro);

    while ($mode eq 'shell') {
        print_color($prompt);

        $input = readline(STDIN);
        chomp $input;

        if ( $input eq 'exit' or $input eq 'quit' ) {
            $mode = 'exit';
            return
        }

        parse($input);
    }
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Shell.pm - Util::Shell module.

=head1 VERSION

This document describes Util::Shell version 0.7.

=head1 NOTE

Util::Shell is a standalone interactive shell and
doesn't require installation of any external libs.

=head1 SYNOPSIS

You can start the Fish interactive shell using the following command:
$ perl sec_check.pl --shell

=head1 ABSTRACT

=head1 DESCRIPTION

Util::Shell module implements an interactive shell application for
querying/checking source libs' vulnerability information in a practical fashion.

=head1 INTERFACE

=over

=item Util::Shell

    * run()

      Definition: Runs command prompt loop during the interactive shell session.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
