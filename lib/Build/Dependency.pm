package Build::Dependency v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Util::IO qw(load_yaml);

use Carp;


sub get_value {
    my $src = shift // '';

    my $table = {
        'win'    => ['win64'],
        'main'   => [ 'fbsd11-amd64', 'generic', 'macos', 'macos-arm64', 'solaris10-i386' ],
        'oldssl' => [ 'aix', 'aix6'],
    };

    if ( exists $table->{$src} ) {
        return $table->{$src};
    } else {
        return '';
    }
}


sub count_freq {
    my $libid    = shift;
    my $ver      = shift;
    my $filepath = shift;

    state $data = Util::IO::read_file($filepath);
    #say "$libid: $ver";

    # regex pattern capture & count method
    my $count = () = $data =~ /$libid\W*$ver/g;

    return $count;
}


sub parse {
    # set YAML dependency constraint file path below
    my $filepath = shift // "$Bin/../data/configs/dependency_constraints.yml";

    # try loading YAML file
    my $deps = load_yaml($filepath)
        or carp "[error] Cannot read dependency constraints file $filepath\n";

    # gather $libinfo and $source_map data
    my @nodes = sort keys %$deps;
    my $len = scalar @nodes;

    my $libid;
    my $ver;
    my $count;

    for my $node ( @nodes ) {
        my ($n, $src) = split ( '-', $node, 2 );
        $source_map{$src} = get_value($src);

        for my $lib (@$deps{$node}) {
            while ( ( $libid, $ver ) = each %$lib ) {
                $count = count_freq( $libid, $ver, $filepath );
                #say "Len: $len, count: $count";

                if ( $len == $count ) {
                    $libinfo{ $libid }{ $ver }{ default } = 1;
                } else {
                    $libinfo{ $libid }{ $ver }{ $src } = 1;
                }
            }
        }
    }

    return $deps;
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Dependency.pm - Build::Dependency module.

=head1 VERSION

This document describes Build::Dependency version 0.7.

=head1 NOTE

This module is developed for loading/parsing the
build dependency constraints config files.

=head1 SYNOPSIS

use Build::Dependency;
...

=head1 DESCRIPTION

Build::Dependency module implements a parser for the
"build dependency constraints" - YAML config file.

=head1 INTERFACE

=over

=item Build::Dependency

    * parse()

      Definition: Loads and parses build dependency constraints config file.

      Input:  ...
      Output: Returns deps hash ref

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
