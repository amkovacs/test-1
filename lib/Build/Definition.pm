package Build::Definition v0.0.7;

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Util::IO qw(load_yaml);

use Carp;


sub parse {
    # set YAML build definition file path below
    my $filepath = shift // "$Bin/../data/configs/jdelog_build_definition_strategicmate.yml";

    # try loading YAML file
    my $builddef = load_yaml($filepath)
        or carp "[error] Cannot read build definition $filepath\n";

    push $source_map{ $_->{(keys(%$_))[0]}{sourceSet} }->@*, (keys(%$_))[0]
        for $builddef->{deps}{binary}->@*;

    # Hashset linking library identifier (buildSource, in terms of security manifest),
    # its version and sourceset
    # e.g. $libinfo{openssl10}{1.0.2u}{oldssl} = 1

    while ( my ( $libid, $ver ) = each $builddef->{deps}{source}{default}->%* ) {
        $ver =~ s/^=//;
        $libinfo{$libid}{$ver}{default} = 1;
    }


    while ( my ( $sset, $libs ) = each $builddef->{deps}{source}{sets}->%* ) {
        while ( my ( $libid, $ver ) = each %$libs ) {
            $ver =~ s/^=//;
            $libinfo{$libid}{$ver}{$sset} = 1;
        }
    }

    return $builddef;
}


1;
__END__

=pod

=encoding utf8

=head1 NAME

Definition.pm - Build::Definition module.

=head1 VERSION

This document describes Build::Definition version 0.7.

=head1 NOTE

This module is developed for loading/parsing the build definition config files.

=head1 SYNOPSIS

use Build::Definition;
...

=head1 DESCRIPTION

Build::Definition module implements a parser for
the old build definition config files.

=head1 INTERFACE

=over

=item Build::Definition

    * parse()

      Definition: Loads and parses build definition config file.

      Input:  ...
      Output: Returns builddef hash ref

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
