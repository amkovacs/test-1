#!/usr/bin/env perl

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use System::OS;
use Cargo::Deny;


sub config_profile {
    my $data = q(
# set rustup PATH
export RUSTUP_HOME="$HOME/.rustup"

# set RUST_SRC_PATH
export RUST_SRC_PATH="$RUSTUP_HOME/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust"

# set cargo HOME
export CARGO_HOME="$HOME/.cargo"

# set cargo PATH
export PATH="$CARGO_HOME/bin:$PATH"
);

    my $profile = "$ENV{HOME}/.profile";
    open(my $file, ">>", $profile);
    say $file $data;

    close $file;
    $file = undef;
}


sub install_cargo_deny {
    my $cmd  = 'cargo --list';
    my $info = System::OS::run($cmd);

    unless ( $info =~ /deny/ ) {
        say '[info] Installing cargo-deny tool...';
        $cmd = 'cargo install --locked cargo-deny';
        my $cargo = System::OS::run($cmd);

        say '[info] Initializing cargo-deny advisories database and configuration...';
        Cargo::Deny::init();
    }
}


sub install_rustup {
    my $cmd = 'rustup';
    my $rustup = System::OS::can_run($cmd);

    unless ( defined( $rustup ) ) {
        say '[info] Installing rustup tool...';
        $cmd = 'bash rustup-init.sh';
        System::OS::run($cmd);
    } else {
        say '[info] Checking rustup updates...';
        $cmd = 'rustup self update';
        $rustup = System::OS::run($cmd);

        $cmd = 'rustup update';
        $rustup = System::OS::run($cmd);
    }
}


sub install_yaml {
    my $distro = System::OS::detect_distro();
    my $cmd;

    eval 'require YAML';

    if ( $@ ) {
        if ( $distro =~ /Debian|Ubuntu/ ) {
            $cmd = 'apt-get -y install libyaml-perl';
        } elsif ( $distro =~ /RedHat|Fedora|CentOS/ ) {
            $cmd = 'dnf install perl-YAML';
        } elsif ( $distro =~ /Arch/ ) {
            $cmd = 'pacman -Sy perl-yaml';
        } elsif ( $distro =~ /FreeBSD/ ) {
            $cmd ='pkg install textproc/p5-YAML';
        } elsif ( $distro =~ /MSYS2/ ) {
            $cmd = 'pacman -Sy perl-YAML';
        } else {
            $cmd = 'cpan -i YAML';
        }

        say '[info] Installing YAML library for Perl...';
        my $info = System::OS::run($cmd);
    }
}


sub print_sys_report {
    my $cmd  = 'rustup show';
    my $info = System::OS::run($cmd);
    say $info;

    $cmd  = 'cargo -Vv';
    $info = System::OS::run($cmd);
    say $info;

    say '[info] All dependencies installed successfully!';
    say '[info] Toolchain configuration done!';
}


sub main {
    say '[info] Checking YAML library for Perl...';
    install_yaml();

    say '[info] Checking rustup tool...';
    install_rustup();

    say '[info] Checking cargo-deny tool...';
    install_cargo_deny();

    say "[info] Configuring '.profile' settings...\n";
    my $cmd = 'rustup';
    my $rustup = System::OS::can_run($cmd);
    config_profile() unless defined $rustup;

    print_sys_report();
}


main();

__END__

=pod

=encoding utf8

=head1 NAME

install.pl - dependency installer for sec_check.

=head1 VERSION

This document describes install.pl script version 0.7.

=head1 NOTE

macOS not supported yet - CPAN installer used instead.

=head1 SYNOPSIS

Use the following command to run this installer script:

$ sudo perl install.pl

=head1 ABSTRACT

=head1 DESCRIPTION

install.pl script installs dependencies (Perl libs) and
the Rust toolchain required by sec_check.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
