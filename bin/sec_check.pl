#!/usr/bin/env perl

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Build::Definition;
use Build::Dependency;
use Cargo::Config;
use Manifest::File;
use System::Cmd;
use System::Dirs;


sub main {
    # parse command line arguments
    System::Cmd::parse();

    # check command line mode
    unless ( $MODE =~ /help|shell/ ) {

        # parse build definition/dependency file
        if ( $ARG  =~ /def/ ) {

            Build::Definition::parse();

        } elsif ( $ARG =~ /dep/ ) {

            Build::Dependency::parse();

        }

        # recursively walk & scan the source dirs for manifest files
        System::Dirs::walk_src($ROOT);

        # check if a dependency doesn't exist in any security manifest
        Manifest::File::check_dep();

    }

    if ( $MODE eq 'cargo' ) {
        # copy cargo-deny configuration file to Rust dir
        # if it doesn't exist

        Cargo::Config::init( $RUST );

        # recursively walk & scan the Rust source dirs for cargo-deny checks
        System::Dirs::walk_rust( $RUST );
    }

    # close open filehandles etc. and exit the program properly
    System::Cmd::sec_exit();
}


main();


__END__

=pod

=encoding utf8

=head1 NAME

sec_check.pl - Command line tool developed for scanning the filesystem and
finding possible vulnerabilities/security issues within the installed libraries.

=head1 VERSION

This document describes sec_check version 0.7.

=head1 NOTE

By default, sec_check.pl saves all the output to log files.
Please check "data/logs" directory to see the log file contents.

=head1 SYNOPSIS

Usage: perl check.pl
    [ --log <filename> |
      --build <dep/def>
      --cargo <offline/online>
      --help | --usage ]

optional arguments:
  --help                Show this help message and exit.
  --build               Parse specified build configuration.
  --cargo               Run cargo-deny checks.
  --rust                Run cargo-deny checks.
  --gendoc              Generate an AsciiDoc document.
  --log                 Generate a log file in plain text format with the list and
                        description of the vulnerabilities affecting jdelog.
  --shell               Start interactive shell session.
  --usage               Show this help message and exit.
```

Use the following command to run sec_check.pl:

$ perl sec_check.pl

This command generates an AsciiDoc report and saves all the outputs to log files by default.
There are 2 different types of log files generated - in "data/logs" directory by default.

* The "report.log" file contains the main security information.

* The "debug.log" file contains detailed debugging information.

Most of the times, the users can check "report.log" file first.
And then they can check "debug.log" file contents to analyze
the security issues in detail.

Starting with sec_check version 0.7,
it's possible to run cargo-deny (online) checks using the following command:

$ perl sec_check.pl --cargo online

In order to run cargo-deny checks in offline mode, use the following command:

$ perl sec_check.pl --cargo offline

To run all tests:

$ prove

To run interactive shell:

$ perl sec_check.pl --shell

=head1 INTERACTIVE SHELL USAGE

The current interactive shell provides a minimal set of builtin commands.
By way of using these commands, the users can query NVD and OSV APIs
in a simpler and more practical fashion.

The list of currently supported interactive commands are as follows:

* cd  [dir]
* ls  [dir]
* cargo.deny [dir]
* hash.get [dir]
* nvd.get  [commit_hash]
* osv.get  [commit_hash]
* pwd
* exit

The set of interactive shell commands are "kept minimal by design" -
as they are sufficient for viewing the directory contents -
and querying the NVD & OSV APIs for a particular library/package source info.

=head1 DESCRIPTION

This command line tool - "sec_check.pl" is developed for scanning the filesystem and
finding possible vulnerabilities/security issues within the library sources -
which affect build.

Please check embedded documentation for the submodules residing inside "/lib" directory for more information.
You can use 'perldoc' utility for displaying the embedded documentation within the respective submodules.

The following command displays embedded documentation for sec_check.pl itself:

$ perldoc sec_check.pl

You can also use perldoc utility to display documentation embedded inside a module:

$ cd lib/OSV

$ perldoc Client.pm

=head1 INTERFACE

=over

=item MAIN

    * main

      Definition: Parses command line parameters, then recursively scans the filesystem
      for installed libraries and manifests. All the planned checks are run sequentially.

=back

=head1 AUTHOR INFORMATION

amk

=head1 BUGS

Please report them.

=cut
