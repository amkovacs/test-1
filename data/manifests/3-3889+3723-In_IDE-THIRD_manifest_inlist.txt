---
name: Netflow dump (nfdump)
cpe23: "cpe:2.3:a::nfdump"
type: embedded
version: 1.6.13
CVEs:
  CVE-2019-1010057:
    status: Not applicable
    description: Affects nfx.c, nffile_inline.c and minilzo.c files which are not part of our code
    reviewDate: 2022-06-07
  CVE-2019-14459:
    status: Not applicable
    description: Affects Process_ipfix_template_withdraw() function which is not present in our code
    reviewDate: 2022-06-16
    comment: See 3889?focusedCommentId=1110763