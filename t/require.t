#!/usr/bin/env perl

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Test::Simple tests => 1;

TEST:
{
    eval 'use YAML';

    ok ( defined ($@), 'Perl-YAML lib requirement test...' );

    if ( $@ ) {
        warn q(
YAML package not found. Please install it to be able to run this script as following:

Ubuntu/Debian: # apt install libyaml-perl
RedHat:        # yum install perl-YAML
Arch:          # pacman -Sy perl-yaml
MSYS2:         \$ pacman -Sy perl-YAML
FreeBSD:       # pkg install textproc/p5-YAML

If packaged version is not available on your system, try running as root:

# cpan -i YAML

);
    }
}


__END__

=pod

=encoding utf8

=head1 NAME

require.t - Perl library requirements check test.

=head1 NOTE

YAML is the only package used which do not reside in core library.
Please see README.md file for more information.

=head1 SYNOPSIS

Use the following command to run this test script:

$ perl require.t

=head1 ABSTRACT

=head1 DESCRIPTION

require.t test script checks if required libraries for
sec_check to run are preinstalled on the current system.

The TEST block runs automatically to check package dependency status.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
