#!/usr/bin/env perl

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Util::IO qw(show_data);

use Build::Dependency;
use Test::Simple tests => 4;


TEST:
{
    my $filepath = "$Bin/../data/configs/dependency_constraints.yml";
    ok ( -f ($filepath), "$filepath path test..." );

    my $deps = Build::Dependency::parse($filepath);
    ok ( defined ($deps), "$filepath load test..." );

    ok ( %source_map, '%source_map data extraction test...' );
    ok ( %libinfo, '%libinfo data extraction test...' );

    say("\n[info] Showing compatibility-layer data...\n");

    my $tag = 'source_map';
    show_data(\%source_map, $tag);

    $tag = 'libinfo';
    show_data(\%libinfo, $tag);
}


__END__

=pod

=encoding utf8

=head1 NAME

dep_parse.t - a test script for Build::Dependency parser module.

=head1 SYNOPSIS

Use the following command to run this test script:

$ perl dep_parse.t

=head1 DESCRIPTION

dep_parse.t test script tests Build::Dependency module functionality.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
