#!/usr/bin/env perl

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Util::IO qw(show_data);

use Build::Definition;
use Test::Simple tests => 4;


TEST:
{
    my $filepath = "$Bin/../data/configs/jdelog_build_definition_strategicmate.yml";
    ok ( -f ($filepath), "$filepath path test..." );

    my $builddef = Build::Definition::parse($filepath);
    ok ( defined ($builddef), "$filepath load test..." );


    ok ( %source_map, '%source_map data extraction test...' );
    ok ( %libinfo, '%libinfo data extraction test...' );

    say("\n[info] Showing compatibility-layer data...\n");

    my $tag = 'libinfo';
    show_data(\%libinfo, $tag);

    $tag = 'source_map';
    show_data(\%source_map, $tag);
}


__END__

=pod

=encoding utf8

=head1 NAME

dep_parse.t - a test script for Build::Definition parser module.

=head1 SYNOPSIS

Use the following command to run dep_parse.t script:

$ perl dep_parse.t

=head1 DESCRIPTION

def_parse.t test script tests Build::Definition module functionality.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
