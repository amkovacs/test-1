#!/usr/bin/env perl

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use NVD::Client qw(get_cves);
use Util::IO qw(show_data);

use Test::Simple tests => 1;


TEST:
{
    # using 'yajl-ruby' CPE string for testing
    my $cpe = 'yajl-ruby' unless defined $ARGV[0];

    my $data = get_cves($cpe);

    ok ( defined ($data), 'JSON data definition test...' );

    say("\n[info] Showing JSON data...\n");

    my $tag = 'JSON';
    show_data($data, $tag);
}


__END__

=pod

=encoding utf8

=head1 NAME

nvd_get.pl - a simple NVD client test.

=head1 SYNOPSIS

You can run the test using the following command:

$ perl nvd_get.t

Or you can supply a command line parameter to test as well:

$ perl nvd_get.t cpe:2.3:a::yajl-ruby

For demonstration, the above command accepts a CPE string -
referring to yajl-ruby library as an example.

You can supply different CPE strings to test the program as well.

=head1 DESCRIPTION

This simple NVD client test is also written for demonstrating how to query NVD API
via HTTP and fetch a particular package CVE data as JSON.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
