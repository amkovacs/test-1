#!/usr/bin/env perl

use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use Cargo::Deny;
use Cargo::Config;

use Cwd qw(abs_path getcwd);
use Test::Simple tests => 2;


TEST:
{
    Cargo::Config::init( $RUST );

    # set a valid Rust source lib dir path below
    my $dir = abs_path( "$RUST/.cargo/registry/src/github.com-1ecc6299db9ec823/gtk-0.9.1" );

    if ( Cargo::Config::valid_lib($dir) ) {
        chdir $dir;
    } else {
        say '[warning]: Not a valid Rust source dir!';
    }

    my $data = Cargo::Deny::check_offline($dir);
    ok ( defined ($data), 'Cargo-deny advisories database offline check...' );

    $data = Cargo::Deny::check_online($dir);
    ok ( defined ($data), 'Cargo-deny advisories database online check...' );
}


__END__

=pod

=encoding utf8

=head1 NAME

cargo_deny.t - Cargo::Deny module test.

=head1 SYNOPSIS

You can run the test using the following command:

$ perl cargo_deny.t

Or you can use 'prove' command line testing tool instead:

$ prove cargo_deny.t

For testing purposes, different Rust source lib directory paths can be supplied -
at the beginning of the TEST block.

=head1 DESCRIPTION

cargo_deny.t test script tests Cargo::Deny module functionality.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
