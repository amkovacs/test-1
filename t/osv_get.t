#!/usr/bin/env perl

# search for Perl modules under the '/lib' directory
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Mod::Base;
use OSV::Client qw(query_commit);
use Util::IO qw(show_data);

use Test::Simple tests => 1;


TEST:
{
    # you can use the following commit hash as an argument -
    # referring to "icu" C/C++ library source (Git repo) as an example
    # '6e5755a2a833bc64852eae12967d0a54d7adf629'

    my $hash = '6e5755a2a833bc64852eae12967d0a54d7adf629' unless defined $ARGV[0];

    my $data = query_commit($hash);

    ok ( defined ($data), 'JSON data definition test...' );

    say("\n[info] Showing JSON data...\n");

    my $tag = 'JSON';
    show_data($data, $tag);
}


__END__

=pod

=encoding utf8

=head1 NAME

osv_get.t - a simple OSV client test.

=head1 SYNOPSIS

You can run the test using the following command:

$ perl osv_get.t

Or you can supply a command line parameter to test as well:

$ perl osv_get.t 6e5755a2a833bc64852eae12967d0a54d7adf629

For demonstration, the above command accepts a commit hash -
referring to "icu" C/C++ library source (Git repo) as an example.

You can supply different parameters from the command line to test the program as well.

=head1 DESCRIPTION

This simple OSV client test is also written for demonstrating how to query OSV API
via HTTP and fetch a particular package vulnerability data as JSON.

=back

=head1 AUTHOR INFORMATION

Koray Eyinç <korayeyinc@gmail.com>

=head1 BUGS

Please report them.

=cut
